import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'sizingpipe' })
export class SizingPipe implements PipeTransform {
  transform(value: string, size: string): string {
    if (value) {
      return (value.replace(/(\/|=)(s)([0-9]{3,})/, "$1$2" + size));
       //value.replace(/\/s[0-9]{3,}\//, "\/s" + size + "\/");

    }
    return value;
  }
}
