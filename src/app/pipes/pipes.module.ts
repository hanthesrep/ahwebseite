import { NgModule } from "@angular/core";
import { SamplePipe } from './sample.pipe';
import { SizingPipe} from './sizing.pipe';
@NgModule({
    imports: [],
    declarations: [SamplePipe, SizingPipe],
    exports: [SamplePipe, SizingPipe]
})
export class PipesModule { }