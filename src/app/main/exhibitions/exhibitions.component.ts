import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { ExhibitonCatalog } from './../../models/exhbitioncatalog';
import { Component, Input, OnInit } from '@angular/core';
import { ContentLoaderService } from 'src/app/services/content-loader.service';
import { map} from 'rxjs/operators';
import { faSpinner, faDice } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-exhibitions',
  templateUrl: './exhibitions.component.html',
  styleUrls: ['./exhibitions.component.scss']
})
export class ExhibitionsComponent implements OnInit {


  @Input()
  exhibitions: string;
  dice = faDice;
  exhibtion$: Observable<ExhibitonCatalog[]>;

  constructor(private contentLoaderService: ContentLoaderService,
  ) { }

  ngOnInit(): void {
    this.shuffle();
  }

  shuffle(){
    this.exhibtion$ = this.contentLoaderService.getExhibitionCatalog(this.exhibitions).pipe(
      map(x => _.sampleSize(x.filter(x => x.display),6)
    ));
  }
}
