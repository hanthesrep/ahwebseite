import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from 'src/app/models/portfolios';
import { ContentLoaderService } from 'src/app/services/content-loader.service';

@Component({
  selector: 'app-portfolios',
  templateUrl: './portfolios.component.html',
  styleUrls: ['./portfolios.component.scss']
})
export class PortfoliosComponent implements OnInit {
  cards$ : Observable<Card[]>;

  constructor(private contentLoaderService: ContentLoaderService,
   ) { }

  ngOnInit(): void {


    this.contentLoaderService.getPortfolios().pipe(
      e => this.cards$  = e
    )
  }

}


