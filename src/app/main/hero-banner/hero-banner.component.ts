import { Component, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { ContentLoaderService } from 'src/app/services/content-loader.service';
import { SamplePipe } from '../../pipes/sample.pipe'

@Component({
  selector: 'app-hero-banner',
  templateUrl: './hero-banner.component.html',
  styleUrls: ['./hero-banner.component.scss']
})
export class HeroBannerComponent implements OnInit {
  
  hero: string;
  tickerSubscribtions: Subscription;

  constructor(private contentLoaderService: ContentLoaderService,
    private samplePipe: SamplePipe) { }

  ngOnInit(): void {
    this.contentLoaderService.getHeros().subscribe(e => { const heros = e; 
    this.tickerSubscribtions = timer(0, 10000).subscribe(t => this.hero = this.samplePipe.transform(heros));
    });
  }

  ngOnDestroy(): void {
    this.tickerSubscribtions.unsubscribe();
  }

}
