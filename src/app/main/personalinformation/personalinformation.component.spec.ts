import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PersonalinformationComponent } from './personalinformation.component';

describe('PersonalinformationComponent', () => {
  let component: PersonalinformationComponent;
  let fixture: ComponentFixture<PersonalinformationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalinformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
