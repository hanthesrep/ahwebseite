import { Component, OnInit } from '@angular/core';
import { faFacebook, faInstagram  } from '@fortawesome/free-brands-svg-icons';
import { faAt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-foot',
  templateUrl: './foot.component.html',
  styleUrls: ['./foot.component.scss']
})
export class FootComponent implements OnInit {
  facebook=faFacebook;
  instagram=faInstagram;
  ad=faAt;

  constructor() { }

  ngOnInit(): void {
  }

}
