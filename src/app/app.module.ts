import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { HeroBannerComponent } from './main/hero-banner/hero-banner.component';
import { ContentLoaderService } from './services/content-loader.service';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from './pipes/pipes.module';
import { PortfoliosComponent } from './main/portfolios/portfolios.component';
import { SamplePipe } from './pipes/sample.pipe';
import { PersonalinformationComponent } from './main/personalinformation/personalinformation.component';
import { CardsComponent } from './main/cards/cards.component';
import { FootComponent } from './main/foot/foot.component';
import { NocookieComponent } from './main/nocookie/nocookie.component';
import { ImpressumComponent } from './static/impressum/impressum.component';
import { DatenschutzComponent } from './static/datenschutz/datenschutz.component';
import { HeaderComponent } from './static/header/header.component';
import { GalleryComponent } from './static/gallery/gallery.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FullscreenheaderComponent } from './static/fullscreenheader/fullscreenheader.component';
import { PreiseComponent } from './static/preise/preise.component';
import { WorkshopComponent } from './static/workshop/workshop.component';
import { ExhibitionComponent } from './static/exhibition/exhibition.component';
import { ContactComponent } from './static/contact/contact.component';
import { ExhibitionsComponent } from './main/exhibitions/exhibitions.component';
import { RandomrotatorDirective } from './directives/randomrotator.directive';
import { ShopComponent } from './shop/shop/shop.component';
import { ShopgalleryComponent } from './shop/shopgallery/shopgallery.component';
import { ShoptcartComponent } from './shop/shoptcart/shoptcart.component';
import { ShopitemComponent } from './shop/shopitem/shopitem.component';
import { SliderComponent } from './common/slider/slider.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HeroBannerComponent,
    PortfoliosComponent,
    PersonalinformationComponent,
    CardsComponent,
    FootComponent,
    NocookieComponent,
    ImpressumComponent,
    DatenschutzComponent,
    HeaderComponent,
    GalleryComponent,
    FullscreenheaderComponent,
    PreiseComponent,
    WorkshopComponent,
    ExhibitionComponent,
    ContactComponent,
    ExhibitionsComponent,
    RandomrotatorDirective,
    ShopComponent,
    ShopgalleryComponent,
    ShoptcartComponent,
    ShopitemComponent,
    SliderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PipesModule,
    FontAwesomeModule,
  ],
  providers: [ContentLoaderService, SamplePipe],
  bootstrap: [AppComponent]
})
export class AppModule {

}
