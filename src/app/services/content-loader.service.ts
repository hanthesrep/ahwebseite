import { element } from 'protractor';
import { ExhibitonCatalog } from './../models/exhbitioncatalog';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Card } from '../models/portfolios'

import { catchError, filter, map } from 'rxjs/operators'
import { throwError } from 'rxjs/internal/observable/throwError';
import { Exhibiton } from '../models/exhibition';

@Injectable({
  providedIn: 'root'
})
export class ContentLoaderService {

  baseurl = 'https://drgonzales.github.io/afw/';

  constructor(private http: HttpClient) { }

  getHeros() {
    const url = this.baseurl +'heros.json';
    return this.http.get<Array<string>>(url);
  }

  getPortfolios() {
    const url = this.baseurl + 'portfolios.json';
    return this.http.get<Card[]>(url);
  }


  getExhibitionCatalog(label?: string) {
    const url = this.baseurl + 'exhibition/exhibitions.json';
    return label ? this.http.get<ExhibitonCatalog[]>(url).pipe(
      map(value => value.filter(element => element.label.includes(label)))) : this.http.get<ExhibitonCatalog[]>(url);
  }


  getExhibition(file: string) {
    const url = this.baseurl + `exhibition/${file}.json`;
    return this.http.get<Exhibiton>(url).pipe(
      catchError(this.handleError)
    );
  }

  getPictures(portfolio: string) {
    const url = `https://www.googleapis.com/blogger/v3/blogs/645477939881106143/posts/search?q=label:${portfolio}&maxResults=5&key=AIzaSyD3h5-cgCmihqtpHSSQpOsob71RPWoa0DY`;
    return this.http.get(url).pipe(
      catchError(this.handleError)
    );
  }


  private handleError(error: HttpErrorResponse) {
    console.error('wir haben ein Problem:', error.error.message);
    return throwError('wir haben ein Problem');
  }

}
