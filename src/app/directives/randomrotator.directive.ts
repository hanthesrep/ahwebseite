import { Directive, ElementRef, HostListener } from '@angular/core';
@Directive({
  selector: '[RandomRotator]'
})
export class RandomrotatorDirective {
  constructor(private el: ElementRef) {

  }

  @HostListener('mouseenter') onMouseEnter() {
    this.el.nativeElement.style.transform = "scale(105%)";

  }

  @HostListener('mouseleave') onMouseLeave() {
    this.el.nativeElement.style.transform = "scale(100%)";
   }

}
