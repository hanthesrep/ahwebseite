import { ElementRef } from '@angular/core';
import { RandomrotatorDirective } from './randomrotator.directive';

describe('RandomrotatorDirective', () => {
  it('should create an instance', () => {
    const el = new ElementRef({});
    const directive = new RandomrotatorDirective(el);
    expect(directive).toBeTruthy();
  });
});
