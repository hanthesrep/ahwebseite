import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { faArrowCircleLeft, faArrowCircleRight, faAngleDoubleUp, faPlay, faSpinner, faArrowLeft, faStopwatch } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs/internal/Subscription';
import { timer } from 'rxjs';
import { Picture } from 'src/app/models/exhibition';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Input()
  pictures: Array<Picture>;
  @Input()
  index: number;
  @Input()
  set run(start: boolean){
      if(start) this.autostart();
  }

  @Output()
  close: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  faSpinner = faSpinner;
  left = faArrowCircleLeft;
  right = faArrowCircleRight;
  home = faAngleDoubleUp;
  starticon = faPlay;
  stopwatch = faStopwatch;
  tickerSubscribtions: Subscription;
  presentation = false;
  loading = false;
  automode = false;

  constructor() { }

  ngOnInit(): void {

  }

  auto(start = 0) {
    this.automode = !this.automode;
    if (!this.automode) {
      this.stopautomode();
      return;
    }

    if (this.automode && (this.tickerSubscribtions == null || this.tickerSubscribtions.closed == true)) {
      this.tickerSubscribtions = timer(start, 3000).subscribe(t => this.next(true));
      return;
    }
  }

  stopautomode() {
    if (this.tickerSubscribtions) {
      this.tickerSubscribtions.unsubscribe();
    }
    this.automode = false;
  }

  next(mode = false) {
    if (this.index < this.pictures.length - 1 && !this.loading) {
      this.loading = true;
      this.index++;
      if (!mode) { this.stopautomode(); }
      return;
    }
    if (mode) {
      if (this.index == this.pictures.length - 1) this.index = 0;
      return;
    }

  }

  back() {
    if (this.index > 0 && !this.loading) {
      this.loading = true;
      this.index--;
    }
    this.stopautomode();
  }

  stop() {
    this.stopautomode();
    this.close.emit(false);
  }

  progress(): number {
    return Math.floor(100 / this.pictures.length * this.index) || 1;
  }

  autostart() {
    this.auto(3000);
  }

  onImageLoad(event) {
    this.loading = false;
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Escape") {
      this.stop();
    }
    if (event.key == "ArrowLeft") {
      this.back();
    }
    if (event.key == "ArrowRight") {
      this.next()
    }
  }

}
