import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FullscreenheaderComponent } from './fullscreenheader.component';

describe('FullscreenheaderComponent', () => {
  let component: FullscreenheaderComponent;
  let fixture: ComponentFixture<FullscreenheaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FullscreenheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullscreenheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
