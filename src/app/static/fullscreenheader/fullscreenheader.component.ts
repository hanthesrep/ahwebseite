import { Component, OnInit, Input } from '@angular/core';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Subscription, timer } from 'rxjs';
import { ContentLoaderService } from 'src/app/services/content-loader.service';
import { SamplePipe } from '../../pipes/sample.pipe'

@Component({
  selector: 'app-fullscreenheader',
  templateUrl: './fullscreenheader.component.html',
  styleUrls: ['./fullscreenheader.component.scss']
})
export class FullscreenheaderComponent implements OnInit {

  tickerSubscribtions: Subscription;
  homeIcon = faArrowLeft;
  hero: string;

  @Input()
  homeNavigation: boolean = true;  //will be show the home navigation

  constructor(private contentLoaderService: ContentLoaderService,
    private samplePipe: SamplePipe) { }

  ngOnInit(): void {
    this.contentLoaderService.getHeros().subscribe(e => {
      const heros = e;
      this.tickerSubscribtions = timer(0, 10000).subscribe(t => this.hero = this.samplePipe.transform(heros));
    });
  }

  ngOnDestroy(): void {
    this.tickerSubscribtions.unsubscribe();
  }


}


