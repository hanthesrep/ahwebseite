import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { Card } from 'src/app/models/portfolios';
import { ContentLoaderService } from 'src/app/services/content-loader.service';
import { faArrowCircleLeft, faArrowCircleRight, faAngleDoubleUp, faPlay, faSpinner, faArrowLeft, faStopwatch, faDice } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs/internal/Subscription';
import { timer } from 'rxjs';
import { Picture } from 'src/app/models/exhibition';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  portfolio: Card;
  picCount = 15;
  pictures: Array<Picture> = [];
  faSpinner = faSpinner;
  dice = faDice;
  presentation = false;
  index=0;

  constructor(private route: ActivatedRoute, private contentLoaderService: ContentLoaderService ) { }

  ngOnInit(): void {
    this.route.params.subscribe(r =>
      this.contentLoaderService.getPortfolios().subscribe(portfolios => {
        this.portfolio = portfolios.filter(t => t.label == r.tag)[0];
        this.getPicUrl(_.sample(this.portfolio.bloggerTags));
      }));
  }

  shuffle() {
    this.getPicUrl(_.sample(this.portfolio.bloggerTags));
  }

  // move to a Service
  getPicUrl(protfolio: string) {
    var result:Array<Picture> = [];

    this.contentLoaderService.getPictures(protfolio).subscribe((data: any) => {
      const findImageLinks = /(src..)\b(https?:\/\/\S+(?:=s{0,4}) | (?:png|jpe?g|gif|JPE?G|PNG|GIF)\S*)\b | (href..)\b(https?:\/\/\S+\S*)\b/g;
      const selectRegexGroup =4;

      if (data && data.items) {
        data.items.forEach(element => (this.getMatches(element.content, findImageLinks, selectRegexGroup))
          .forEach(url => result.push({url:url,description:this.portfolio.titel + " " + this.portfolio?.subtitel})));
        this.pictures = _.sampleSize(result,this.picCount);
      } else {
        this.pictures = null;
      }
    });
  }

  private getMatches(string, regex, index = 1) {
    const matches = [];
    let match: any[];
    while (match = regex.exec(string)) { //Moppelkotz, das muss schöder werden
      if (match[index].endsWith('jpg') | match[index].endsWith('JPG') | match[index].endsWith('JPEG') | match[index].endsWith('jpeg') | match[index].includes('blogger.googleusercontent.com')){
      matches.push(match[index]);
      }
    }
    return matches;
  }

  closeing(event){
    this.presentation = false;
  }

  open(i = 0) {
    this.index = i;
    this.presentation = true;
  }

}


