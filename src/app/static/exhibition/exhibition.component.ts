import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Exhibiton } from 'src/app/models/exhibition';
import { ContentLoaderService } from 'src/app/services/content-loader.service';
import { faArrowCircleLeft, faArrowCircleRight, faAngleDoubleUp, faPlay, faSpinner, faArrowLeft, faStopwatch } from '@fortawesome/free-solid-svg-icons';
import { Subscription, timer } from 'rxjs';



@Component({
  selector: 'app-exhibition',
  templateUrl: './exhibition.component.html',
  styleUrls: ['./exhibition.component.scss']
})
export class ExhibitionComponent implements OnInit {

  starticon = faPlay;
  homeIcon = faArrowLeft;
  exhibition: Exhibiton;
  index = 0;
  presentation = false;
  autorun = false;

  constructor(private route: ActivatedRoute, private contentLoaderService: ContentLoaderService) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(r => this.contentLoaderService.getExhibition(r.tag))
    ).subscribe(c => this.exhibition = c);
  }

  closeing(event){
    this.presentation = false;
  }

  start(i = 0) {
    this.index = i;
    this.presentation = true;
  }

  autostart(){
    this.autorun=true;
    this.start();
  }
}
