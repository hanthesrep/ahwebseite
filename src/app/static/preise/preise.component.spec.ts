import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PreiseComponent } from './preise.component';

describe('PreiseComponent', () => {
  let component: PreiseComponent;
  let fixture: ComponentFixture<PreiseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PreiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
