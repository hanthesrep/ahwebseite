import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ShopComponent } from './shop/shop/shop.component';
import { DatenschutzComponent } from './static/datenschutz/datenschutz.component';
import { ExhibitionComponent } from './static/exhibition/exhibition.component';
import { GalleryComponent } from './static/gallery/gallery.component';
import { ImpressumComponent } from './static/impressum/impressum.component';
import { PreiseComponent } from './static/preise/preise.component';
import { WorkshopComponent } from './static/workshop/workshop.component';


const routes: Routes = [
  { path: 'start', component: MainComponent },
  { path: 'impressum', component: ImpressumComponent },
  { path: 'datenschutz', component: DatenschutzComponent },
  { path: 'preise', component: PreiseComponent },
  { path: 'workshop', component: WorkshopComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'gallery/:tag', component: GalleryComponent },
  { path: 'exhibition/:tag', component: ExhibitionComponent },
  { path: '**', redirectTo: 'start', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
