import { ExhibitonCatalog } from './../../models/exhbitioncatalog';
import { ContentLoaderService } from '../../services/content-loader.service';
import { Observable } from 'rxjs';
import { Exhibiton } from '../../models/exhibition';
import { Component, Input, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-shopgallery',
  templateUrl: './shopgallery.component.html',
  styleUrls: ['./shopgallery.component.scss']
})
export class ShopgalleryComponent implements OnInit {

  @Input()
  exhibition:ExhibitonCatalog;
  items$:Observable<Exhibiton>;
    constructor(private contentLoaderService:ContentLoaderService) { }

  ngOnInit(): void {
    this.items$ = this.contentLoaderService.getExhibition(this.exhibition.exibition);
  }
}
