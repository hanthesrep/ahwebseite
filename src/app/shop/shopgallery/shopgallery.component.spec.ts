import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShopgalleryComponent } from './shopgallery.component';

describe('ShopgaleryComponent', () => {
  let component: ShopgalleryComponent;
  let fixture: ComponentFixture<ShopgalleryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopgalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopgalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
