import { Injectable } from '@angular/core';
import { Picture, Print } from '../models/exhibition';

@Injectable({
  providedIn: 'root'
})
export class ShopingcartService {

  cartItems: {item:Picture; print:Print}[] = [];

  public addItem(item:Picture, print:Print){
    this.cartItems.push({item:item,print:print});
    console.dir(this.cartItems);
  }
  constructor() { }
}
