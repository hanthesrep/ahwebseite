import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ExhibitonCatalog } from 'src/app/models/exhbitioncatalog';
import { ContentLoaderService } from 'src/app/services/content-loader.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  exhibtion$: Observable<ExhibitonCatalog[]>;

  constructor(private contentLoaderService: ContentLoaderService,
  ) { }

  ngOnInit(): void {
    this.contentLoaderService.getExhibitionCatalog().pipe(e => this.exhibtion$ = e)
  }
}
