import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShoptcartComponent } from './shoptcart.component';

describe('ShoptcartComponent', () => {
  let component: ShoptcartComponent;
  let fixture: ComponentFixture<ShoptcartComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoptcartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoptcartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
