import { ShopingcartService } from './../shopingcart.service';
import { Picture } from './../../models/exhibition';
import { Component, Input, OnInit } from '@angular/core';
import { Print } from 'src/app/models/exhibition';

@Component({
  selector: 'app-shopitem',
  templateUrl: './shopitem.component.html',
  styleUrls: ['./shopitem.component.scss']
})
export class ShopitemComponent implements OnInit {
  @Input()
  item: Picture;

  constructor(private cart: ShopingcartService) { }

  ngOnInit(): void {
  }

  addItem(item: Picture, print: Print) {
    this.cart.addItem(item, print);
  }

}
