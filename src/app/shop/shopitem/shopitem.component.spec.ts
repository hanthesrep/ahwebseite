import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShopitemComponent } from './shopitem.component';

describe('ShopitemComponent', () => {
  let component: ShopitemComponent;
  let fixture: ComponentFixture<ShopitemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
