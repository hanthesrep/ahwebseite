import { Picture, Print } from './../models/exhibition';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShoptcartService {
  cartItems: [{item:Picture; print:Print}];
  constructor() { }

  public addItem(item:Picture, print:Print){
    this.cartItems.push({item:item,print:print});
    console.dir(this.cartItems);
  }
}
