export interface Card {
  titel: string;
  subtitel?:string;
  label: string;
  exhibition: string;
  pictureUrls: string[];
  bloggerTags: string[];
  contentup?:string[];
  contentdown?:string[];
}
