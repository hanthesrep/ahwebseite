export interface Picture {
  url: string;
  description: string;
  prints?: Print;
}

export interface Print {
  format: string;
  price: number;
}
export interface Exhibiton {
  titel: string;
  titelPicture: string;
  content: string[];
  pictures: Picture[];
}
