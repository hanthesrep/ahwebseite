export interface ExhibitonCatalog {
  titel: string;
  preview: string;
  content: string[];
  exibition: string; //Name der Exhbition.json
  label: string[]; // zum Gruppieren und Filtern für die Darstellung einer z.b Gallery
  display:boolean; // true zeigt alles, false nur im shop
}
